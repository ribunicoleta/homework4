import React, {Component} from 'react'

class RobotForm extends Component{
    constructor(props){
        super(props)
        this.state={
            name:'',
            type:'',
            mass:''
        }
        this.handleChange=(evt)=>{
            this.setState({
                [evt.target.name]:evt.target.value
            })
        }
        this.add=()=>{
            this.props.onAdd({
                name:this.state.name,
                type:this.state.type,
                mass:this.state.mass
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="name" name="name" />
            <input type="text" placeholder="type" name="type" />
            <input type="text" placeholder="mass"name="mass" />
            <input type="button" value="add" onClick={this.add}/>
        </div>
            
    }
}

export default RobotForm